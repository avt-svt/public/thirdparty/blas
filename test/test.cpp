 /**
 * @file test.cpp
 * 
 * @brief Test file for blas.
 * 
 * ==============================================================================\n
 * © Aachener Verfahrenstechnik-Systemverfahrenstechnik, RWTH Aachen University  \n
 * ==============================================================================\n
 * 
 *
 * @author Dominik Bongartz, Jaromil Najman, Alexander Mitsos
 * @date 29.09.2018
 * 
 */

#include <iostream>
#include "blasNameMangling.h"
#define dscal FCBLAS_GLOBAL(dscal,DSCAL)


extern "C" {
	void dscal(const long&, const double&, double*, const long&);
}


int main() {

	int N = 3;
	double array[3];
	for(int i=0; i<N; ++i) {
		array[i] = i;
	}
	std::cout << std::endl;
	for(int i=0; i<N; ++i) {
		std::cout << array[i] << std::endl;
	}
	std::cout << std::endl;
	
	dscal(N,5.0,array,1);
	for(int i=0; i<N; ++i) {
		std::cout << array[i] << std::endl;
	}
	std::cout << std::endl;
	
	std::cout << "BLAS seems to work." << std::endl << std::endl;
	
	return 0;
	
}