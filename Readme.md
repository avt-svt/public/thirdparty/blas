The folder blas-3.8.0 contains BLAS version 3.8.0, downloaded from www.netlib.org/blas on January 21, 2019.
The only additions are the other files in this folder, the test folder, and the pre-compiled binaries for Visual Studio 2017.
For licensing information, see file Credit.txt.

This blas version has been tested for the following configurations:
	- Windows 7 using Microsoft Visual Studio 2017
	- Windows 7 using MinGW with gcc 4.9.3
	- Red Hat Linux 8.2.1 using gcc 8.2.1
	- OS X 10.9.5 using clang-600.0.57 and gfortran 4.9.0
